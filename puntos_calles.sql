-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2018 a las 01:55:14
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `puntos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntos_calles`
--

CREATE TABLE `puntos_calles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `latitud` varchar(80) NOT NULL,
  `longitud` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puntos_calles`
--

INSERT INTO `puntos_calles` (`id`, `nombre`, `latitud`, `longitud`) VALUES
(5, 'Udabol', '-17.390844611503866', ' -66.06878766878668'),
(6, 'Udabol', '-17.390028814539196', ' -66.07270744823825'),
(7, 'Udabol', '-17.39020185598089', ' -66.07560794585459'),
(8, 'villa', '-17.39040150538484', ' -66.07754986518137');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `puntos_calles`
--
ALTER TABLE `puntos_calles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `puntos_calles`
--
ALTER TABLE `puntos_calles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
